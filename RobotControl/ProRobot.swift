//
//  ProRobot.swift
//  RobotControl
//
//  Created by Admin on 06/05/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import AudioToolbox

class ProRobot: UIViewController {
    
    var send = UDPSend()
    var command = ""
    
    @IBAction func button1(_ sender: Any) {
        command = "1"
        commandFunc()
    }
    @IBAction func button2(_ sender: Any) {
        command = "2"
        commandFunc()
    }
    @IBAction func button3(_ sender: Any) {
        command = "3"
        commandFunc()
    }
    @IBAction func button4(_ sender: Any) {
        command = "4"
        commandFunc()
    }
    @IBAction func button5(_ sender: Any) {
        command = "5"
        commandFunc()
    }
    @IBAction func button6(_ sender: Any) {
        command = "6"
        commandFunc()
    }
    @IBAction func button7(_ sender: Any) {
        command = "7"
        commandFunc()
    }
    @IBAction func button8(_ sender: Any) {
        command = "8"
        commandFunc()
    }
    @IBAction func button9(_ sender: Any) {
        command = "9"
        commandFunc()
    }
    @IBAction func button10(_ sender: Any) {
        command = "10"
        commandFunc()
    }
    @IBAction func button11(_ sender: Any) {
        command = "11"
        commandFunc()
    }
    @IBAction func button12(_ sender: Any) {
        command = "12"
        commandFunc()
    }
    @IBAction func button13(_ sender: Any) {
        command = "13"
        commandFunc()
    }
    @IBAction func button14(_ sender: Any) {
        command = "14"
        commandFunc()
    }
    
    @IBAction func button16(_ sender: Any) {
        command = "16"
        commandFunc()
        //performSegue(withIdentifier: "pro", sender: self)
        navigationController?.popViewController(animated: true)
        
    }

    @IBAction func button19(_ sender: Any) {
        command = "19"
        commandFunc()
    }

    @IBAction func button25(_ sender: Any) {
        command = "25"
        commandFunc()
    }
    @IBAction func button26(_ sender: Any) {
        command = "26"
        commandFunc()
    }
    
    func commandFunc(){
        send.send(host: Model.host, port: Model.port, message: command)
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
}

