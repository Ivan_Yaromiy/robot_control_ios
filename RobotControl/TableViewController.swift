//
//  RobotSelectionTableController.swift
//  RobotControl
//
//  Created by Admin on 17.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    var item: ItemForTable?
    
    @IBAction func pushAddAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Create new Robot", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addTextField { (textFieldName) in
            textFieldName.placeholder = "Name"
        }
        
        alert.addTextField { (textFieldHost) in
            textFieldHost.placeholder = "Host"
        }
        
        alert.addTextField { (textFieldPort) in
            textFieldPort.placeholder = "Port"
        }
        
        let alertActionCreate = UIAlertAction(title: "Create", style: UIAlertActionStyle.default) { (alertAction) in
            if ((alert.textFields![0].text != "") && (alert.textFields![1].text != "") && (alert.textFields![2].text != "")) {
                let newItem = ItemForTable(name: alert.textFields![0].text!, port: Int(alert.textFields![2].text!)!, host: alert.textFields![1].text!)
                self.item?.addArrayItem(arrayItem: newItem)
                self.tableView.reloadData()
                saveData()
            }
        }
        
        let alertActionCancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (alert) in
            
        }

        alert.addAction(alertActionCreate)
        alert.addAction(alertActionCancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if item == nil {
            item = rootItem
        }

        let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGestureRecognizer.minimumPressDuration = 1
        tableView.addGestureRecognizer(longPressGestureRecognizer)
    }

    @objc func handleLongPress(longPress: UILongPressGestureRecognizer) {
        
        let pointOfTouch = longPress.location(in: tableView)
        let indexPath = tableView.indexPathForRow(at: pointOfTouch)
        
        if longPress.state == .began {
            if let indexPath = indexPath {
                let item = self.item?.arrayItems[indexPath.row]
                
                let alert = UIAlertController(title: "Edit Robot", message: "", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addTextField { (textFieldName) in
                    textFieldName.placeholder = "Name"
                    textFieldName.text = item?.name
                }
                
                alert.addTextField { (textFieldHost) in
                    textFieldHost.placeholder = "Host"
                    textFieldHost.text = item?.host
                }
                
                alert.addTextField { (textFieldPort) in
                    textFieldPort.placeholder = "Port"
                    textFieldPort.text! = String(describing: item!.port)
                }
                
                let alertActionCreate = UIAlertAction(title: "Update", style: UIAlertActionStyle.default) { (alertAction) in
                    if ((alert.textFields![0].text != "") && (alert.textFields![1].text != "") && (alert.textFields![2].text != "")) {
                        item?.name = alert.textFields![0].text!
                        item?.port = Int(alert.textFields![2].text!)!
                        item?.host = alert.textFields![1].text!

                        self.tableView.reloadData()
                        saveData()
                    }
                }
                
                let alertActionCancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (alert) in
                    
                }
                
                alert.addAction(alertActionCreate)
                alert.addAction(alertActionCancel)
                
                present(alert, animated: true, completion: nil)            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return item!.arrayItems.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell

        let itemForCell = item!.arrayItems[indexPath.row]
        
        cell.initCell(item: itemForCell)
        
        return cell
    }
    

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
 

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            item?.removeArrayItem(index: indexPath.row)
            saveData()
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
