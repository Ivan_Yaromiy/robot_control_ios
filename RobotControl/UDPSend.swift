//
//  UDPSend.swift
//  RobotControl
//
//  Created by Admin on 06/05/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import SwiftSocket

class UDPSend {
    
    var client: UDPClient?
    
    func send(host: String, port: Int, message: String) { 

        //let host = "192.168.0.255"
        //let port = 8888
        client = UDPClient(address: String(host) , port: Int32(port)  )
        //client?.send(string: message)
                print("\(message)")
        //guard let client = client else { return }
 
        if let response = sendRequest(string: message, using: client!) {
            print("Response: \(response)")
        }
    }
    private func sendRequest(string: String, using client: UDPClient) -> String? {
        
        switch client.send(string: string) {
        case .success:
            return "success"
        case .failure(let error): print(error)
            return nil
        }
    }
}

