//
//  TirminalRobot.swift
//  RobotControl
//
//  Created by Admin on 30/05/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class TerminalRobot: ViewController {
    
    var send = UDPSend()
    
    @IBOutlet weak var terminalText: UITextField!
    @IBOutlet weak var terminalView: UITextView!
    
    @IBAction func sendUDP(_ sender: Any) {
        terminalView.text! += "\(terminalText.text!)\n"
        terminalView.scrollRangeToVisible(terminalView.selectedRange)
        send.send(host: Model.host, port: Model.port, message: terminalText.text!)
    }
  
}
