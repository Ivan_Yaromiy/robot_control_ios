//
//  UDPSettings.swift
//  RobotControl
//
//  Created by Admin on 06/05/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class UDPSettings: UIViewController {
    
    var hostIP = "", hostIP1 = "", hostIP2: String = ""
    var port = 0, port1 = 0, port2 = 0
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var textIP1: UITextField!
    @IBOutlet weak var textPort1: UITextField!
    @IBOutlet weak var textIP2: UITextField!
    @IBOutlet weak var textPort2: UITextField!
    @IBOutlet weak var outSwitch1: UISwitch!
    @IBOutlet weak var outSwitch2: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
/*
        outSwitch1.isOn = defaults.bool(forKey: "switch1")
        outSwitch2.isOn = defaults.bool(forKey: "switch2")
        textIP1.text! = defaults.string(forKey: "host1")!
        textPort1.text! = defaults.string(forKey: "port1")!
        textIP2.text! = defaults.string(forKey: "host2")!
        textPort2.text! = defaults.string(forKey: "port2")!
 */
    }

    @IBAction func switch1(_ sender: Any) {
        if ((sender as AnyObject).isOn == true){
            outSwitch1.isOn = true
            outSwitch2.isOn = false
            defaults.set(outSwitch1.isOn, forKey: "switch1")
            defaults.set(outSwitch2.isOn, forKey: "switch2")
            defaults.synchronize()
            hostIP1 = textIP1.text!
            port1 = Int(textPort1.text!)!
            defaults.set(hostIP1, forKey: "host1")
            defaults.set(port1, forKey: "port1")
            defaults.synchronize()
        } else {
            outSwitch1.isOn = false
            outSwitch2.isOn = true
            defaults.set(outSwitch1.isOn, forKey: "switch1")
            defaults.set(outSwitch2.isOn, forKey: "switch2")
            defaults.synchronize()
            hostIP2 = textIP2.text!
            port2 = Int(textPort2.text!)!
            defaults.set(hostIP1, forKey: "host2")
            defaults.set(port1, forKey: "port2")
        }
    }
    @IBAction func switch2(_ sender: Any) {
        if ((sender as AnyObject).isOn == true){
            outSwitch1.isOn = false
            outSwitch2.isOn = true
            defaults.set(outSwitch1.isOn, forKey: "switch1")
            defaults.set(outSwitch2.isOn, forKey: "switch2")
            defaults.synchronize()
            hostIP2 = textIP2.text!
            port2 = Int(textPort2.text!)!
            defaults.set(hostIP2, forKey: "host2")
            defaults.set(port2, forKey: "port2")
            defaults.synchronize()
        } else {
            outSwitch1.isOn = true
            outSwitch2.isOn = false
            defaults.set(outSwitch1.isOn, forKey: "switch1")
            defaults.set(outSwitch2.isOn, forKey: "switch2")
            defaults.synchronize()
            hostIP1 = textIP1.text!
            port1 = Int(textPort1.text!)!
            defaults.set(hostIP1, forKey: "host1")
            defaults.set(port1, forKey: "port1")
            defaults.synchronize()
        }
        
    }
   // var send = UDPSend()
    
    @IBAction func savingButton(_ sender: Any) {

        if outSwitch1.isOn == true {
            hostIP = textIP1.text!
            port = Int(textPort1.text!)!
        } else {
            hostIP = textIP2.text!
            port = Int(textPort2.text!)!
        }
        print("HostSave \(hostIP) + portSave \(port)")
        hostIP1 = textIP1.text!
        port1 = Int(textPort1.text!)!
        defaults.set(hostIP1, forKey: "host1")
        defaults.set(port1, forKey: "port1")
        hostIP2 = textIP2.text!
        port2 = Int(textPort2.text!)!
        defaults.set(hostIP2, forKey: "host2")
        defaults.set(port2, forKey: "port2")
        defaults.set(hostIP, forKey: "host")
        defaults.set(port, forKey: "port")
        defaults.synchronize()

    }
    
}

