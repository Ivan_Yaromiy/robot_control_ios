//
//  BasicRobot.swift
//  RobotControl
//
//  Created by Admin on 06/05/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import AudioToolbox


class BasicRobot: UIViewController {

    @IBOutlet weak var commandLabel: UILabel!

    var send = UDPSend()
    var command = ""
    
    @IBAction func button1(_ sender: Any) {
        command = "1"
        commandFunc()
    }
    @IBAction func button2(_ sender: Any) {
        command = "2"
        commandFunc()
    }
    @IBAction func button3(_ sender: Any) {
        command = "3"
        commandFunc()
    }
    @IBAction func button4(_ sender: Any) {
        command = "4"
        commandFunc()
    }
    @IBAction func button5(_ sender: Any) {
        command = "5"
        commandFunc()
    }
    @IBAction func button6(_ sender: Any) {
        command = "6"
        commandFunc()
    }
    @IBAction func button7(_ sender: Any) {
        command = "7"
        commandFunc()
    }
    @IBAction func button8(_ sender: Any) {
        command = "8"
        commandFunc()
    }
    @IBAction func button9(_ sender: Any) {
        command = "9"
        commandFunc()
    }
    @IBAction func button10(_ sender: Any) {
        command = "10"
        commandFunc()
    }
    @IBAction func button11(_ sender: Any) {
        command = "11"
        commandFunc()
    }
    @IBAction func button12(_ sender: Any) {
        command = "12"
        commandFunc()
    }
    @IBAction func button13(_ sender: Any) {
        command = "13"
        commandFunc()
    }
    @IBAction func button14(_ sender: Any) {
        command = "14"
        commandFunc()
    }
    @IBAction func button15(_ sender: Any) {
        command = "15"
        commandFunc()
    }
    @IBAction func button16(_ sender: Any) {
        command = "16"
        commandFunc()
        //performSegue(withIdentifier: "backBasic", sender: self)
        navigationController?.popToRootViewController(animated: true)
        
    }
    @IBAction func button17(_ sender: Any) {
        command = "17"
        commandFunc()
    }
    @IBAction func button18(_ sender: Any) {
        command = "18"
        commandFunc()
    }
    @IBAction func button19(_ sender: Any) {
        command = "19"
        commandFunc()
    }
    @IBAction func button20(_ sender: Any) {
        command = "20"
        commandFunc()
    }
    @IBAction func button21(_ sender: Any) {
        command = "21"
        commandFunc()
    }
    @IBAction func button22(_ sender: Any) {
        command = "22"
        commandFunc()
    }
    @IBAction func button23(_ sender: Any) {
        command = "23"
        commandFunc()
    }
    @IBAction func button24(_ sender: Any) {
        command = "24"
        commandFunc()
    }
    @IBAction func button25(_ sender: Any) {
        command = "25"
        commandFunc()
    }
    @IBAction func button26(_ sender: Any) {
        command = "26"
        commandFunc()
    }
    @IBAction func button27(_ sender: Any) {
        command = "27"
        commandFunc()
    }
    @IBAction func button28(_ sender: Any) {
        command = "28"
        commandFunc()
    }
    @IBAction func button29(_ sender: Any) {
        command = "29"
        commandFunc()
    }
    @IBAction func button30(_ sender: Any) {
        command = "30"
        commandFunc()
    }
    @IBAction func button31(_ sender: Any) {
        command = "31"
        commandFunc()
    }
    @IBAction func button32(_ sender: Any) {
        command = "32"
        commandFunc()
    }
    
    func commandFunc(){
        
        send.send(host: Model.host, port: Model.port, message: command)
        commandLabel.text! = command
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

