//
//  Model.swift
//  RobotControl
//
//  Created by Admin on 16.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

var pathForSaveData: String {
    let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.libraryDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]+"/data.plist"
    
    print(path)
    
    return path
}

var rootItem: ItemForTable?

func loadData() {
    if let dict = NSDictionary(contentsOfFile: pathForSaveData) {
        rootItem = ItemForTable(dictionary: dict)
    } else {
        rootItem = ItemForTable(name: "Root", port: 0000, host: "Root")
    }
}

func saveData() {
    rootItem?.dictionary.write(toFile: pathForSaveData, atomically: true)
}




class Model {

    static var port: Int = 8888
    static var host: String = "1.2.3.4"
    
}
