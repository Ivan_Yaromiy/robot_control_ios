//
//  ItemForTable.swift
//  RobotControl
//
//  Created by Admin on 17.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class ItemForTable {
    
    var name: String
    var port: Int
    var host: String
    var isComplete: Bool
    
    var arrayItems: [ItemForTable]
    
    init(name: String, port: Int, host: String){
        
        self.name = name
        self.port = port
        self.host = host
        self.isComplete = false
        
        self.arrayItems = []
    }
    
    init(dictionary: NSDictionary) {
        self.name = dictionary.object(forKey: "name") as! String
        self.port = dictionary.object(forKey: "port") as! Int
        self.host = dictionary.object(forKey: "host") as! String
        self.isComplete = dictionary.object(forKey: "isComplete") as! Bool
        
        self.arrayItems = []
        
        let array = dictionary.object(forKey: "array") as! NSArray
        for arrayDict in array {
            self.arrayItems.append(ItemForTable(dictionary: arrayDict as! NSDictionary))
        }
    }
    
    var dictionary: NSDictionary {
        var array = NSArray()
        for item in arrayItems {
            array = array.adding(item.dictionary) as NSArray
        }

        let dictionary = NSDictionary(objects: [name, port, host, isComplete, array], forKeys: ["name" as NSCopying,"port" as NSCopying,"host" as NSCopying,"isComplete" as NSCopying,"array" as NSCopying])
        
        return dictionary
    }
    
    func addArrayItem(arrayItem: ItemForTable) {
        arrayItems.append(arrayItem)
    }
    
    func removeArrayItem(index: Int){
        arrayItems.remove(at: index)
    }
    
    func changeState() {
        isComplete = !isComplete
    }
}
