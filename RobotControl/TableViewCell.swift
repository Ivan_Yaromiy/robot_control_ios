//
//  TableViewCell.swift
//  RobotControl
//
//  Created by Admin on 24.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
 
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var hostLabel: UILabel!
    @IBOutlet weak var portLabel: UILabel!

    @IBOutlet weak var buttonCheck: UIButton!
    @IBAction func pushCheckAction(_ sender: Any) {

        var sdt = itemInCell!.arrayItems
        
        itemInCell?.changeState()
        
        setCheckButton()
    }
    
    var itemInCell: ItemForTable?
    
    func initCell(item: ItemForTable) {
        itemInCell = item
        nameLabel.text = itemInCell?.name
        hostLabel.text = itemInCell?.host
        portLabel.text = String(describing: itemInCell!.port)
    
        setCheckButton()
    }
    
    func setCheckButton() {
        if itemInCell!.isComplete {
            buttonCheck.setImage(UIImage(named: "check.png"), for: .normal)
        } else {
            buttonCheck.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
    }
    
}
